import std/enumutils
import hand
import rank
import suit

import localize

type
  Combination* = enum
    nada
    pair
    twopair
    three
    straight
    full
    flush
    four
    quinte
    fourjack


proc int*(combination: Combination): int =
  result = combination.symbolRank


proc `$`*(combination: Combination): string =
  @[tr"Nada", tr"One Pair", tr"Two pair",
    tr"Three of a kind", tr"Straight", tr"Full house",
    tr"Flush", tr"Four of a kind", tr"Quinte flush",
    tr"Four jack "][combination.symbolRank]


proc getCombination*(hand: Hand): Combination =
  ## Return hand combination
  result = nada
  let suits = hand.suitsCounter()
  let ranks = hand.ranksCounter()
  if suits[0][1] == 5:
    if hand.isStraight():
      result = quinte
    else:
      result = flush
  elif ranks[0][1] == 4:
    if ranks[0][0] == jack:
      result = fourjack
    else:
      result = four
  elif ranks[0][1] == 2:
    if ranks[1][1] == 2:
      result = twopair
    else:
      result = pair
  elif ranks[0][1] == 3:
    if ranks[1][1] == 2:
      result = full
    else:
      result = three
  elif hand.isStraight:
    result = straight


proc getLevel*(hand: Hand): int =
  let combination: Combination = hand.getCombination()
  if combination == nada:
    return 0

  var
    sortedHand: Hand = hand
    handRanks = hand.ranksCounter()
    rank1: Rank = handRanks[0][0]
    rank2: Rank = handRanks[1][0]
    suit: Suit

  if combination in @[flush, quinte, straight]:
    if combination == straight:
      sortedHand = hand.sortedRank()
    suit = sortedHand[^1].suit
    rank1 = sortedHand[^1].rank
    rank2 = seven # int = 0
  elif combination == twopair:
    let r1: Rank = max(rank1, rank2)
    let r2: Rank = min(rank1, rank2)
    rank1 = r1
    rank2 = r2
    suit = hand.maxSuit(rank1)
  elif combination in @[three, full]:
    suit = hand.maxSuit(rank1)
    if combination != full:
      rank2 = seven
  elif combination == pair:
    suit = hand.maxSuit(rank1)
    rank2 = seven

  result = 256 * combination.symbolRank + int(rank1) * 32 + int(rank2) * 4 +
      int(suit)
