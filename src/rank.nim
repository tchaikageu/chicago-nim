import std/enumutils

type
  Rank* = enum
    seven = "7"
    eight = "8"
    nine = "9"
    ten = "10"
    jack = "J"
    queen = "Q"
    king = "K"
    ace = "A"


proc int*(rank: Rank): int =
  ## Return rank value
  result = rank.symbolRank


proc `<`*(rank1, rank2: Rank): bool =
  int(rank1) < int(rank2) or int(rank1) == int(rank2)

const ranksString*: string =
  block:
    var s: string
    for rank in Rank:
      s = s & $rank & " "
    s[0..^2]
