import std/random
import std/deques
import std/sequtils
import std/strformat

import std/os

import argparse
import localize
import termstyle
import termuiNG

import card
import combination
import deck
import grid
import hand
import player
import utils


const version {.strdefine.}: string = ""

var
  playingDeck: Deque[Card] = initDeque[Card](deckLen)


proc playersChangeCards(grid: Grid, players: seq[Player]) =
  for player in players:
    var getCard: Deque[Card]

    if grid.isShitty(player):
      echo red $player & " " & tr"can’t change [shit]"
      continue

    if player.isBot():
      sleep(rand(800..1200))
    else:
      echo blue bold tr"{player}'s hand" & fmt": {$player.hand.sortedRank()} - {player.hand.getCombination()}"

    let level = player.hand.getLevel()
    var cards = player.changeCards()

    if cards.len == 0:
      echo $player & ": " & tr"No cards thanks"
    elif cards.len == 1:
      let deckCard = playingDeck.getCards()
      echo $player & ": " & tr"One card please"
      echo blue tr"First card" & fmt": {deckCard[0]}"
      playingDeck.addLast(cards[0])

      if player.keepCard(deckCard[0], level):
        player.hand.addCards(deckCard)
        echo fmt"{player}: " & tr"Yes please"
      else:
        playingDeck.addLast(deckCard[0])
        var card = playingDeck.getCards()
        player.hand.addCards(card)
        echo fmt"{player}: " & tr"No thank you."
        if not player.isBot():
          echo tr"You got" & fmt": {card[0]}"

    else:
      echo fmt"{player}: {cards.len} " & tr"cards please"
      for card in cards:
        playingDeck.addLast(card)
        getCard = playingDeck.getCards()
        player.hand.addCards(getCard)
    if not player.isBot() and cards.len > 0:
      echo blue bold tr"{player}'s hand" & fmt": {$player.hand.sortedRank()} - {player.hand.getCombination()}"
      waitKey()

proc checkCombWinner(players: seq[Player]): void =
  var
    bestLevel: int = 0
    bestPlayer: Player
    points: int

  for player in players:
    let level = player.hand.getLevel()

    if level > bestLevel:
      bestPlayer = player
      bestLevel = level

  if bestLevel != 0:
    echo red tr"The combination winner is {bestPlayer} with" & " " &
    $bestPlayer.hand.getCombination() & " " &
    "(" & $bestLevel & ")"
    points = int(bestPlayer.hand.getCombination())
    bestPlayer.score += points
    echo red tr"{bestPlayer} wins {points} points"
    if bestPlayer.hand.getCombination() == fourjack:
      echo red bold tr"Four Jacks for {bestPlayer}"
      for player in players:
        if player != bestPlayer:
          player.score = 0
          echo red "* " & $player & " score: 0"
  else:
    echo red tr"No one wins"
  echo ""
  waitKey()


proc firstRound(grid: Grid, players: var seq[Player], initial_players: seq[
    Player]): void =
  # first round
  echo green bold "\n## " & tr"First round" & " ##"
  echo italic tr"The dealer is" & fmt" {players[^1]}"
  playersChangeCards(grid, players)
  printHandGrid(players, true)
  checkCombWinner(players)
  grid.printGrid(initial_players)
  playersChangeCards(grid, players)


proc secondRound(grid: Grid, players: var seq[Player], initial_players: seq[
    Player]): void =
  # Second round
  var hands: seq[Hand]
  var savedPlayers: seq[Player] = players

  echo green bold "\n## " & tr"Second round" & " ##"
  if grid.anyShitty(players):
    echo tr"No Chicago possible !"
  else:
    for player in players:
      var answer = player.doChicago()
      if answer:
        echo green $player & ": Chicago"
        player.chicago = true
      else:
        echo $player & ": " & tr"No Chicago"
        player.chicago = false

  echo ""

  # Save hands
  for player in players:
    hands.add(player.hand)

  for turn in 0..4:
    var turnWinner: Player
    var number: int = 0
    var winnerIdx: int = 0
    var firstCard, bestCard: Card
    var tmpPlayers: seq[Player]
    var playedCards: seq[Card]

    for player in players:
      var card: Card = player.playCard(playedCards)
      playedCards.add(card)
      if number == 0:
        firstCard = card
        bestCard = card
        turnWinner = player
      else:
        if card > bestCard:
          bestCard = card
          turnWinner = player
          winnerIdx = number

      echo $player & ": " & $card
      number += 1
    echo red tr"Turn winner is {turnWinner} with" & fmt" {bestCard}" & "\n"
    tmpPlayers = concat(players[winnerIdx..players.len-1], players[
        0..winnerIdx-1])
    players = tmpPlayers
  echo green tr"The round winner is {players[0]}" & " +5 " & tr"points"
  players[0].score += 5

  for player in players:
    if player.chicago:
      if player == players[0]:
        echo green tr"{player} wins 5 points more for winning Chicago"
        player.score += 5
      else:
        echo red tr"{player} looses 5 points for failing Chicago"
        player.score -= 5

  players = savedPlayers

  var number = 0
  # Restore players order and hands
  for player in players:
    player.hand = hands[number]
    number += 1
    player.chicago = false

  printHandGrid(players, false)
  checkCombWinner(players)
  grid.printGrid(initial_players)


proc getIntValue(option: string): int =
  try:
    result = parseInt(option)
  except ValueError:
    echo red fmt"{option} " & tr"is not a Int"
    echo tr"Please run command with -h to see help"
    quit(1)


proc play() =
  var
    players: seq[Player]
    initial_players: seq[Player]
    chicGrid: Grid
    bots: seq[string] = @["Mick", "Paul", "Nicky"]
    humanName: string = "Joe"
    dontAskName: bool = false
    nbPlayers: int
    lines: int
    p = newParser("Chicago"):
      flag("-d", "--dontaskname", help = "Don't ask your name")
      option("-l", "--lines", default = some("6"),
          help = "Set grid size [4..10]")
      option("-n", "--name", help = "Set your name")
      option("-p", "--players", default = some("4"),
          help = "Number of players [2..4]")
      flag("-v", "--version", help = "Show version info and exit",
          shortcircuit = true)
  try:
    let opts = p.parse()
    dontAskName = opts.dontaskname
    if opts.name != "":
      humanName = opts.name
      if humanName.len > 12:
        humanName.delete(11..humanName.len-1)
        humanName &= ".."
      dontAskName = true
    lines = getIntValue(opts.lines)
    nbPlayers = getIntValue(opts.players)
    if nbPlayers < 2:
      nbPlayers = 2
    elif nbPlayers > 4:
      nbPlayers = 4

  except ShortCircuit as e:
    if e.flag == "argparse_help":
      echo p.help
      quit(0)
    elif e.flag == "version":
      echo blue "\nChicago v." & $version & "\n"
      quit(0)
  except UsageError:
    stderr.writeLine red getCurrentExceptionMsg()
    quit(1)

  chicGrid = initGrid(lines = lines)
  playingDeck.initDeck()

  echo ""
  const welcome = "Chicago - The cards game !"
  echo blue frameIt welcome
  echo "v." & $version
  echo ""

  if not dontAskName:
    let question = tr"Hello. What’s your name?"
    humanName = termuiAsk(question, defaultValue = humanName)
  players.add(initPlayer("human", humanName))

  for botName in bots:
    if players.len < nbPlayers:
      players.add(initPlayer("robot", botName))

  players.shuffle()
  initial_players = players
  var playAgain: bool = true

  while playAgain:
    while not chicGrid.hasWinner(players):
      var firstPlayer: Player = players[0]
      playingDeck.deckCut()
      # Cards dealing
      for i in 0..4:
        for player in players:
          player.hand.addCards(playingDeck.getCards())
      firstRound(chicGrid, players, initial_players)
      if chicGrid.hasWinner(players):
        break
      secondRound(chicGrid, players, initial_players)
      for player in players:
        playingDeck.handToDeck(player.hand)
      var tmpPlayers: seq[Player] = players[1..players.len-1]
      tmpPlayers.add(firstPlayer)
      players = tmpPlayers
    var winner: Player
    var bestScore: int = 0
    for player in players:
      if player.score > bestScore:
        bestScore = player.score
        winner = player

    echo green frameIt($winner & " " & tr"wins the game!")
    echo ""
    for player in players:
      player.score = 0
      playingDeck.handToDeck(player.hand)

    playingDeck.deckCut()
    playAgain = termuiConfirm(tr"Play again ?")
    echo ""

  echo bold "\n" & tr"Have a nice day ;)"

when isMainModule:
  requireLocalesToBeTranslated ("fr", "")
  #requireLocalesToBeTranslated ("es", "")
  #requireLocalesToBeTranslated ("de", "")
  updateTranslations()
  globalLocale[0] = systemLocale()
  randomize()
  play()
