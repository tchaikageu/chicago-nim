import std/enumutils

type
  Suit* = enum
    clubs = "♣"
    diamonds = "♢"
    hearts = "♡"
    spades = "♠"


proc int*(suit: Suit): int =
  ## Return suit value
  result = suit.symbolRank
