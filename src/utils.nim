import std/strutils
import std/terminal

import termstyle
import localize

proc frameIt*(msg: string, symb: char = '*'): string =
  ## TODO: improve this code
  let l = msg.len + 6
  let frame: string = repeat(symb, l)
  result.add(frame)
  result.add("\n")
  result.add(repeat(symb, 2))
  result.add(" ")
  result.add(msg)
  result.add(" ")
  result.add(repeat(symb, 2))
  result.add("\n")
  result.add(frame)


proc clearLine*(): void =
  stdout.cursorUp(1)
  stdout.eraseLine()


proc waitKey*(): void =
  echo blue tr"Hit any key to resume"
  discard getch()
  clearLine()
