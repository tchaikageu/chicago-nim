#import std/deques
import card
import hand
import rank
import suit


proc keepSuits*(hand: Hand, suits: seq[Suit]): seq[seq[Card]] =
  # Keep given suits
  # returns @[keptcards,discarded]
  var keptCards, discarded: seq[Card]

  for card in hand:
    if card.suit in suits:
      keptCards.add(card)
    else:
      discarded.add(card)

  result = @[keptCards, discarded]


proc keepRanks*(hand: Hand, ranks: seq[Rank]): seq[seq[Card]] =
  # Keep given ranks
  # returns @[keptcards,discarded]
  var
    keptCards, discarded: seq[Card]

  for card in hand:
    if card.rank in ranks:
      keptCards.add(card)
    else:
      discarded.add(card)

  result = @[keptCards, discarded]


proc keepCards*(hand: Hand, cards: seq[Card]): seq[seq[Card]] =
  # Keep given cards
  # returns @[keptcards,discarded]
  var keptCards, discarded: seq[Card]
  for card in hand:
    if card in cards:
      keptCards.add(card)
    else:
      discarded.add(card)

  result = @[keptCards, discarded]
