import std/algorithm
import std/deques
import std/sequtils
import strutils
import card
import suit
import rank


type
  Hand* = seq[Card]


proc addCards*(hand: var Hand, cards: Deque[Card]): void =
  for c in cards:
    hand.add(c)


proc dropCards*(hand: var Hand, cards: seq[Card]) =
  if cards.len == 0:
    return
  var newHand: Hand
  var keep: bool
  for handCard in hand:
    keep = true
    for droppedCard in cards:
      if handCard == droppedCard:
        keep = false
    if keep:
      newHand.add(handCard)

  hand = newHand


proc compareCounts[T](a, b: (T, int)): int =
  if b[1] < a[1] or a[1] == b[1]: -1
  else: 1


proc countSuit*(hand: Hand, suit: Suit): int =
  result = hand.filterIt(it.suit == suit).len


proc suitsCounter*(hand: Hand): seq[tuple[suit: Suit, count: int]] =
  ## Return sequence of [Suit, count] if count != 0
  var nb: int
  var count: tuple[suit: Suit, count: int]
  for suit in Suit:
    nb = hand.countSuit(suit)
    count = (suit: suit, count: nb)
    if nb != 0:
      result.add(count)
  result.sort(compareCounts)


proc countRank*(hand: Hand, rank: Rank): int =
  ## Counts number of cards with the given rank
  result = hand.filterIt(it.rank == rank).len


proc ranksCounter*(hand: Hand): seq[tuple[rank: Rank, count: int]] =
  ## Return sequence of [Rank, count] if count != 0i
  var nb: int
  var count: tuple[rank: Rank, count: int]
  for rank in Rank:
    nb = hand.countRank(rank)
    if nb != 0:
      count = (rank: rank, count: nb)
      result.add(count)
  result.sort(compareCounts)


proc rankList*(hand: Hand): seq[Rank] =
  ## Return sequence of Rank
  for card in hand:
    result.add(card.rank)
  result.sort


proc sortSuit*(hand: var Hand): void =
  ## Sort hand by suits
  hand.sort(compareSuit)


proc sortedSuit*(hand: Hand): Hand =
  result = hand
  sortSuit(result)


proc sortRank*(hand: var Hand): void =
  ## Sort hand by ranks in place
  hand.sort(compareRank)


proc sortedRank*(hand: Hand): Hand =
  result = hand
  sortRank(result)


proc isStraight*(hand: Hand): bool =
  var handRanks: string
  for rank in hand.rankList():
    handRanks = handRanks & $rank & " "
  handRanks = handRanks[0..^2]
  result = handRanks in ranksString


proc getStraightsList*(hand: var Hand): seq[Hand] =
  var list: seq[Hand]
  hand.sortRank()
  var
    index: int = 0
    lastIndex: int
  while index < hand.len:
    lastIndex = index + 1
    while lastIndex < hand.len:
      if isStraight(hand[index .. lastIndex]):
        lastIndex += 1
      else:
        break
    list.add(hand[index .. lastIndex-1])
    index = lastIndex

  return list


proc maxSuit*(hand: Hand, rank: Rank): Suit =
  var matched: seq[Suit]
  for card in hand:
    if card.rank == rank:
      matched.add(card.suit)
  result = max(matched)

proc playableCards*(hand: Hand, playedCards: seq[Card]): Hand =
  var cards: Hand
  var firstCard: Card

  if playedCards.len == 0 or hand.len == 1:
    return hand.sortedRank()

  firstCard = playedCards[0]

  for card in hand:
    if card.suit == firstCard.suit:
      cards.add(card)
  if len(cards) == 0:
    result = hand.sortedRank()
  else:
    result = cards.sortedRank()

proc bestCard*(cards: seq[Card]): Card =
  if cards.len != 0:
    var firstCard: Card = cards[0]
    result = firstCard
    for card in cards:
      if card > result:
        result = card
