import std/deques
import std/random

import card
import rank
import suit


const deckLen* = (Rank.high.ord + 1) * (Suit.high.ord + 1)

proc deckShuffle*(deck: var Deque[Card]): void =
  ## Shuffe the deck
  var d: seq[Card]
  for card in deck:
    d.add(card)
  d.shuffle
  deck = d.toDeque

proc deckCut*(deck: var Deque[Card]): void =
  var cutter = rand(1..deck.len-1)
  for _ in 0..cutter:
    deck.addFirst(deck.popLast())

proc initDeck*(deck: var Deque[Card]) =
  for suit in Suit:
    for rank in Rank:
      deck.addLast(Card(rank: rank, suit: suit))
  deck.deckShuffle()

proc getCards*(d: var Deque[Card], number: int = 1): Deque[Card] =
  ## Give cards to player
  for i in 1..number:
    result.addFirst(d.popFirst)


proc handToDeck*(deck: var Deque, hand: var seq[Card]): void =
  ## Returns cards to deck
  for i in 0..hand.len - 1:
    deck.addFirst(hand[i])
  hand = @[]
