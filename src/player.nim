import std/random
import std/os
import std/strformat

import localize

import card
import combination
import hand
import rank
import split
import suit
import termuiNG
import utils


type
  Player* = ref object of RootObj
    name*: string
    hand*: Hand
    score*: int
    chicago*: bool

  PlayerHuman = ref object of Player
  PlayerBot* = ref object of Player


proc `$`*(player: Player): string =
  player.name


method playCard*(player: Player, playedCards: seq[Card]): Card {.base.} =
  discard

method doChicago*(player: Player): bool {.base.} =
  result = false

method isBot*(player: Player): bool {.base.} =
  return true


method isBot*(player: PlayerHuman): bool =
  return false


proc nearStraight(hand: var Hand): seq[Card] =
  var
    straights = hand.getStraightsList()
  if isStraight(hand):
    result = @[]
  elif isStraight(hand[0 .. len(hand) - 2]):
    result = @[hand[^1]]
  elif isStraight(hand[1 .. len(hand) - 1]):
    result = @[hand[0]]
  elif straights.len == 2:
    let
      first = straights[0]
      second = straights[1]
    if int(first[^1].rank) == int(second[0].rank) - 2:
      # case: @[[9♣, 10♠, J♡], [K♣, A♢]]
      result = @[straights[0][0]]
    elif first[^1].rank == second[0].rank:
      result = @[first[^1]]
    elif straights[0].len == 2:
      result = straights[0]
    elif first[^1].rank == second[0].rank:
      # case: @[[10♣, J♠, Q♢], [Q♡, K♣]]
      result = @[first[^1]]
    else:
      result = straights[1]
  elif straights.len == 3:
    if straights[0].len == 3 and straights[1].len == 3 and straights[2].len == 1:
    # this case: @[[7♣], [10♠, J♡, Q♢], [A♠]]
      result = straights[0]
    else:
      result = @[]

  else:
    result = @[]


proc dropCards*(player: Player, cards: seq[Card]) =
  player.hand.dropCards(cards)


method changeCards*(player: Player): seq[Card] {.base.} =
  var
    hand = player.hand
    cards: seq[Card]

  let
    nearStraight: seq[Card] = nearStraight(hand)
    ranks = hand.ranksCounter()
    suits = hand.suitsCounter()
    combination = hand.getCombination()

  if combination >= straight:
    cards = @[]
  elif ranks[0][1] == 3:
    let
      rank1: Rank = ranks[0][0]
      rank2: Rank = ranks[1][0]
    cards = hand.keepRanks(@[rank1, rank2])[1]

  elif ranks[0][1] == 2 and ranks[1][1] == 2:
    let rank = @[ranks[0][0], ranks[1][0]]
    cards = hand.keepRanks(rank)[1]

  elif suits[0][1] == 4:
    let suit: Suit = suits[0][0]
    cards = hand.keepSuits(@[suit])[1]

  elif nearStraight.len > 0:
    cards = nearStraight

  elif suits[0][1] == 3:
    let suit: Suit = suits[0][0]
    cards = hand.keepSuits(@[suit])[1]

  elif ranks[0][1] == 2:
    cards = (hand.keepRanks(@[ranks[0][0], ace]))[1]

  else:
    cards = (hand.keepRanks(@[ace, king]))[1]

  #result = player.hand.dropCards(cards)
  result = cards
  player.hand.dropCards(cards)


method changeCards*(player: PlayerHuman): seq[Card] =
  var options: seq[Card]
  for card in player.hand:
    options.add(card)

  var cards = termuiSelectMultiple(tr"Select cards to change" & fmt" {player}", options)
  clearLine()
  result = cards
  player.hand.dropCards(cards)


method keepCard*(player: Player, card: Card, level: int): bool {.base.} =
  true


method keepCard*(player: PlayerHuman, card: Card, level: int): bool =
  result = termuiConfirm(tr"Keep" & fmt" {card} {player} ?")
  clearLine()


method keepCard*(player: PlayerBot, card: Card, level: int): bool =
  sleep(rand(500..808))
  if card.rank == ace:
    return rand(0..10) > 5
  var newHand = player.hand
  newHand.add(card)
  if newHand.getLevel > level and newHand.getCombination() != pair:
    return true
  result = false

method doChicago*(player: PlayerBot): bool =
  var minForChicago: int = 12
  let combination: Combination = player.hand.getCombination()
  let ranks = player.hand.ranksCounter()
  sleep(rand(850..1200))
  for card in player.hand:
    if card.rank == ace:
      minForChicago -= 2

  if combination == quinte:
    minForChicago -= 1
  if combination == flush:
    minForChicago -= 1
  if combination == full:
    if ranks[0][0] == ace:
      minForChicago -= 5
    elif ranks[1][0] == ace:
      minForChicago -= 2

  return rand(0..10) >= minForChicago


method doChicago*(player: PlayerHuman): bool =
  result = termuiConfirm(tr"Do you claim Chicago" & fmt" {player} ?")
  clearLine()

method playCard(player: PlayerHuman, playedCards: seq[Card]): Card =
  var options: seq[Card]
  var playableCards: Hand = player.hand.playableCards(playedCards)
  var playedCard: Card
  let hand = player.hand

  for card in playableCards:
    options.add(card)
  if playableCards.len == 1:
    playedCard = playableCards[0]
    echo tr"Only one playable card" & " (" & tr"your hand" &
        fmt": {hand} ) : {playedCard}"
    waitKey()
  else:
    var msg: string = tr"Select card to play"
    msg &= " (" & tr"your hand" & ": " & $hand & ")"
    playedCard = termuiSelect(msg, options)
  clearLine()
  result = playedCard
  player.dropCards(@[playedcard])


method playCard(player: PlayerBot, playedCards: seq[Card]): Card =
  var playableCards = player.hand.playableCards(playedCards)
  var card: Card = playableCards[0]
  var aceRank: seq[Card]
  if playedCards.len == 0:
    for card in playableCards:
      if card.rank == ace:
        aceRank.add(card)
    if aceRank.len > 0:
      if rand(0..10) > 3:
        card = aceRank[rand(0..aceRank.len-1)]
  else:
    if playableCards[^1] > bestCard(playedCards) and rand(0..10) > 4:
      card = playableCards[^1]

  player.dropCards(@[card])
  result = card
  sleep(rand(850..1200))


proc initPlayer*(kind: string, name: string): Player =
  case kind
  of "human":
    return PlayerHuman(name: name)
  of "robot":
    return PlayerBot(name: name)
  else:
    raise newException(ValueError, fmt"Unknown player kind: {kind}")
