import localize
import nancy
import termstyle

import combination
import hand
import player
import random
import utils

type
  Grid* = ref object
    size*: int
    shits*: seq[int]


proc initGrid*(lines: int = 6): Grid =
  result = Grid(size: lines-1, shits: @[])
  if lines < 4:
    result.size = 3
  if lines > 10:
    result.size = 9

  case result.size
  of 3..4:
    result.shits = @[1]
  of 5..6:
    result.shits = @[1, 3]
  of 7:
    result.shits = @[1, 3, 5]
  else:
    let shits = [@[2, 4, 6], @[1, 4, 6], @[1, 3, 6], @[1, 3, 5]]
    result.shits = shits[rand(0..3)]


proc isShitty*(grid: Grid, player: Player): bool =
  var score = player.score
  for shit in grid.shits:
    if score in shit*10+1..shit*10+10:
      return true
  return false


proc anyShitty*(grid: Grid, players: seq[Player]): bool =
  for player in players:
    if grid.isShitty(player):
      return true
  return false


proc hasWinner*(grid: Grid, players: seq[Player]): bool =
  for player in players:
    if player.score >= grid.size * 10 + 1:
      return true
  return false


proc getTableRow(grid: Grid, scores: seq[int], line: int): seq[string] =
  let minScore: int = line * 10 + 1
  var
    pointsString: string
    symb: string = "|"

  for score in scores:
    if line in grid.shits:
      symb = red "/"
    elif line == grid.size:
      symb = green "|"
    else:
      symb = "|"

    if score >= minScore:
      for point in minScore..minScore+9:
        if score >= point:
          pointsString &= symb
        if point mod 5 == 0 and point mod 10 != 0:
          pointsString &= " "
    result.add(pointsString)
    pointsString = ""
  if line in grid.shits:
    result.add(red "*") # Shit zone
  else:
    if line == grid.size:
      result.add(green "W")
    else:
      result.add("") # Not a shit zone

proc setHeaders*(t: var TerminalTable, s: seq[string]) =
  var nSeq: seq[string]
  for h in s:
    nSeq.add(bold blue h)
  t.add(nSeq)


proc printGrid*(grid: Grid, players: seq[Player]): void =
  var
    t: TerminalTable
    headers: seq[string]
    scores: seq[int]

  for player in players:
    headers.add(blue $player & " (" & $player.score & ")")
    scores.add(player.score)

  headers.add("S")

  t.setHeaders(headers)
  for line in 0..grid.size:
    t.add(grid.getTableRow(scores, line))

  t.echoTableSeps(seps = boxSeps)
  waitKey()


proc printHandGrid*(players: seq[Player], hide: bool): void =
  var t: TerminalTable

  t.setHeaders @[tr"Name", tr"Hand", tr"Combination", tr"Level"]

  for player in players:
    var row: seq[string]

    row.add(bold player.name)

    if hide and player.isBot():
      row.add(tr"HIDDEN")
    else:
      row.add($player.hand.sortedRank())
    row.add($player.hand.getCombination())
    row.add(green $player.hand.getLevel())

    t.add(row)

  echo ""
  t.echoTableSeps(seps = boxSeps)
