import rank
import suit


type
  Card* = ref object
    rank*: Rank
    suit*: Suit


proc `$`*(card: Card): string =
  ## Return string representaion of Card
  result = $(card.rank) & $(card.suit)


proc `>`*(card1, card2: Card): bool =
  ## Compare if is same suit and then ranks
  (card1.suit == card2.suit) and (int(card1.rank) > int(card2.rank))


proc `<`*(card1, card2: Card): bool =
  not (card1 > card2)


proc `|`*(card1, card2: Card): bool =
  ## Compare cards rank
  card1.rank < card2.rank


proc compareRank*(card1, card2: Card): int =
  ## Compare cards ranks
  if card1.rank < card2.rank:
    result = -1
  else:
    result = 1


proc compareSuit*(card1, card2: Card): int =
  if card1.suit < card2.suit:
    return 1
  if card1.suit == card2.suit:
    return compareRank(card1, card2)
  return -1
