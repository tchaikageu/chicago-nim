# Package

version = "0.1.4"
author = "Tchaikageu"
description = "The Chicago cards game in nim"
license = "GPLv3"
srcDir = "src"
bin = @["chicago"]

import strformat


let opts = &"--verbose -d:lto -d:version=\"{version}\""


# Dependencies

requires "nim >= 1.6.14"
requires "argparse, termstyle, nancy, elvis, classes, localize, termuiNG >= 0.0.11"

task chicago, "Make the game":
  exec &"nimble build {opts}"

task danger, "Build for danger":
  exec &"nimble build -d:danger -d:strip --opt:size {opts}"

task release, "Build for release":
  exec &"nimble build -d:release -d:strip --opt:size {opts}"

task debug, "Build for debug":
  exec &"nimble build -d:debug --debugger:native -y --styleCheck:hint {opts}"

task tests, "Run tests":
  exec "nim r tests/testcomb.nim"
