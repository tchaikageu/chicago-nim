# chicago-nim

Terminal implemention of the Chicago cards game in nim

Check chicago-rules-fr.md or chicago-rules-en.md for playing rules

## Requirements:

- nim ≥ 1.6.14 (not tested with older version)
- argparse
- classes
- elvis
- localize
- termstyle
- nancy
- [termuiNG](https://git.envs.net/tchaikageu/nim-termuiNG)


## Installation instructions

```
# termuiNG installation
nimble install https://git.envs.net/tchaikageu/nim-termuiNG
git clone https://git.envs.net/tchaikageu/chicago-nim.git chicago-nim
cd chicago-nim
nimble install
```

## Usage:

```
Chicago [options]

Options:
  -h, --help
  -d, --dontaskname          Don't ask your name
  -l, --lines=LINES          Set grid size [4..10]
  -n, --name=NAME            Set your name
  -p, --players=PLAYERS      Number of players [2..4]
  -v, --version              Show version info and exit
```
## Using locales

For example if you want to play in French:

```
LANG="fr" ./chicago
```


## Running game on MS Windows

Although the Chicago game is developed in linux environment (Termux on my phone), you can compile and run it on Windows systems but, with default encoding, cmd.exe can't display all cards suits. You have to change default cmd's encoding ([read here](http://stackoverflow.com/questions/57131654/ddg#57134096)) for better UTF-8 support. Or you can use an alternative terminal app with built-in UTF-8 support, like the awesome [tabby](https://tabby.sh/) app to play the game.

