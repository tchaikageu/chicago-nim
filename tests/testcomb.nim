import termstyle

import ../src/combination
import ../src/card
import ../src/suit
import ../src/rank


let straightRanks = @[@[seven, eight, nine, ten, jack], @[eight, nine, ten,
    jack, queen], @[nine, ten, jack, queen, king], @[ten, jack, queen, king, ace]]


proc testNada =
  echo "** Testing Nada...[TODO]"


proc genPairsSeq*(rank: Rank): seq[seq[Card]] =
  var
    firstIndex = 0
    secondIndex = 1
  for suit1 in Suit:
    while secondIndex <= 3:
      result.add(@[Card(rank: rank, suit: suit1), Card(rank: rank, suit: Suit(secondIndex))])
      secondIndex += 1
    firstIndex += 1
    secondIndex = firstIndex + 1


proc testPair =
  echo "** Testing Pair...[TODO]"


proc genTwoPairSeq: seq[seq[Card]] =
  var
    firstIndex = 0
    secondIndex = 1
  for rank in Rank:
    for p in genPairsSeq(rank):
      while secondIndex <= int(ace):
        var twoPair: seq[Card] = p
        for p2 in genPairsSeq(Rank(secondIndex)):
          twoPair.add(p2)
          result.add(@[twoPair])
          twoPair = p
        secondIndex += 1
      secondIndex = firstIndex + 1
    firstIndex += 1
    secondIndex = firstIndex + 1


proc testTwoPair =
  echo blue "** Testing Two Pair..."
  var pairsSeq = genTwoPairSeq()
  for twoPairs in pairsSeq:
    for rank in Rank:
      if rank notin @[twoPairs[0].rank, twoPairs[2].rank]:
        for suit in Suit:
          var hand = twoPairs
          hand.add(Card(rank: rank, suit: suit))
          assert(hand.getCombination == twopair)
  echo green "> OK"


proc genTreeOfSeq*(rank: Rank) =
  discard


proc testTree =
  echo "** Testing Tree Of A Kind...[TODO]"


proc testStraight* =
  echo "** Testing Sraight...[TODO]"


proc genFull*(rank1, rank2: Rank) =
  discard


proc testFull =
  echo "** Testing Full House...[TODO]"

proc testFlush =
  echo blue "** Testing Flush..."
  for suit in Suit:
    var hand: seq[Card]
    for rank in Rank:
      hand.add(Card(rank: rank, suit: suit))
    for index in 0 .. 4:
      var flushHand: seq[Card] = hand[0..index]
      if index == 4:
        flushHand = flushHand[0..flushHand.high-1]
        flushHand.add(hand[hand.high])
      else:
        for card in hand[index+2..5]:
          flushHand.add(card)
    hand = @[]
  echo green "> OK"


proc genFour(rank: Rank): seq[Card] =
  for suit in Suit:
    result.add(Card(rank: rank, suit: suit))


proc testFour =
# Also tests Four Jack combination
  echo blue "** Testing Four of a kind..."
  for rank in Rank:
    let fourOf = genFour(rank)
    for suit in Suit:
      for rank2 in Rank:
        var hand = fourOf
        if rank2 != rank:
          hand.add(Card(rank: rank2, suit: suit))
          if rank == jack:
            assert(hand.getCombination == fourjack)
          else:
            assert(hand.getCombination == four)
        hand = fourOf
  echo green "> OK"


proc testQuinte =
  echo blue "** Testing Quinte..."
  for straightHand in straightRanks:
    var hand: seq[Card]
    for suit in Suit:
      for rank in straightHand:
        hand.add(Card(rank: rank, suit: suit))
      assert(hand.getCombination() == quinte)
      hand = @[]
  echo green "> OK"


proc testFourJack =
  echo blue "** Testing Four Jack"
  let jacks = genFour(jack)
  for rank in Rank:
    var hand = jacks
    if rank != jack:
      for suit in Suit:
        hand.add(Card(rank: rank, suit: suit))
        assert(hand.getCombination == fourjack)
        hand = jacks
  echo green "> OK"


testNada()
testPair()
testTwoPair()
testTree()
testStraight()
testFull()
testFlush()
testFour()
testQuinte()
testFourJack()
